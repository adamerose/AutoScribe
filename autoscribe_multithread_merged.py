import sys

import scipy
from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5 import QtTest
import time
import threading
import autoscribe_output
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import matplotlib.style as ms
import pyaudio
import time
import sys
from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.QtWidgets import *
from abjad import *
import abjad
import music_window
import realtime_music_window
from tkinter.filedialog import *
from PyQt5.QtCore import pyqtSignal
import librosa
import librosa.display
import copy
#import mytimer

#mt = mytimer.MyTimer()

TIME_CHUNK = 0.5
DOTS_PER_SECOND = 20


# Note Object
class Create_Note():
    def __init__(self, letter, start_location):
        # Create a note object and set its parameters
        self.letter = letter
        self.start_location = start_location
        self.x = 0
        self.y = 0

    def __init__(self, letter):
        # Create a note object and set its parameters
        self.letter = letter
        self.start_location = None
        self.x = 0
        self.y = 0


# Window GUI Object
class Music_Window(QWidget):  # QWidget
    # Define Class Variables
    note_index = 0  # Index for array of notes/lables

    # Constants regarding pixel positions
    C4_POSITION = 444
    DELTA_Y = 12.5
    DELTA_X = 13.75
    RIGHT_SIDE = 932
    NOTE_SIZE = 30
    BORDER_PIXEL = 40  # Delete notes left of this pixel value

    # Rendering every recorded sample would scroll the GUI too fast. So we render 1 in RENDER_RATIO samples
    RENDER_RATIO = 1000
    # Wait time after add_note in ms
    # WAIT_TIME = 1 second / [ CHUNK SIZE / RENDER RATIO ]
    WAIT_TIME = 0  # 1000*TIME_CHUNK*RENDER_RATIO / (44100)

    note_count_global = 0  # add note count

    # Initialize music window
    def __init__(self):
        super(Music_Window, self).__init__()
        self.initUI()

    # Configure the default settings of the MusicWindow object
    def initUI(self):
        TEMPLATE_PATH = r'Resources\piano_roll.png'

        # Window Settings
        self.setWindowTitle('AutoScribe')
        self.setWindowIcon(QtGui.QIcon(r"Resources\logo.ico"))  # Icon
        self.setGeometry(100, 100, 942, 640)  # Windows size

        # Display a background image
        sheet_image = QtWidgets.QLabel(self)
        pixmap = QtGui.QPixmap(TEMPLATE_PATH)
        sheet_image.setPixmap(pixmap)
        sheet_image.move(0, 0)

        # display buttons:
        self.record_button = QPushButton('Record', self)
        self.record_button.setToolTip('Start realtime recording')
        self.record_button.move(200, 605)
        self.record_button.clicked.connect(self.record_btn)

        # print automatically when finish recording?
        self.stop_button = QPushButton('Stop Recording', self)
        self.stop_button.setToolTip('Start realtime recording')
        self.stop_button.move(400, 605)
        self.stop_button.clicked.connect(self.stop_btn)
        self.stop_button.setDisabled(True)

        self.select_wav_button = QPushButton('Transcribe File', self)
        self.select_wav_button.setToolTip('Transcribe music file to pdf')
        self.select_wav_button.move(600, 605)
        self.select_wav_button.clicked.connect(self.select_wav_btn)
        # self.select_wav_button.setDisabled(True)

        # remove button and print automatically when finish recording?
        self.print_button = QPushButton('Print to pdf', self)
        self.print_button.setToolTip('Print recorded music to pdf')
        self.print_button.move(800, 605)
        self.print_button.clicked.connect(self.print_btn)
        self.print_button.setDisabled(True)
        self.show()

        # Array of note labels and objects
        self.note_fulllist = []
        self.note_label_list = []
        self.note_obj_list = []

        # Worker Thread
        # start processing thread

        self.worker_thread = WorkerThread()
        self.worker_thread.mySignal.connect(self.on_change)

    # button functions:
    # realtime recording
    def record_btn(self):
        self.worker_thread.recording_thread = True;
        self.worker_thread.start()
        self.record_button.setDisabled(True)
        self.stop_button.setDisabled(False)
        self.select_wav_button.setDisabled(True)
        self.print_button.setDisabled(True)
        self.note_fulllist = []

    # stop recording
    def stop_btn(self):
        self.stop_button.setDisabled(True)
        self.print_button.setDisabled(False)
        self.record_button.setDisabled(False)
        self.select_wav_button.setDisabled(False)
        self.worker_thread.stop()

    # select wav file and transcribe it to pdf
    def select_wav_btn(self):
        print("select wav")
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        file_name, _ = QFileDialog.getOpenFileName(self, "QFileDialog.getOpenFileName()", "",
                                                  "All Files (*);;Python Files (*.py)", options=options)
        note_list = []
        dur_list = []
        if(file_name != ""):
            self.worker_thread.recording_thread = False
            self.worker_thread.filename = file_name
            self.worker_thread.start()

        else:
            print("enter valid file")

    # print real time recorded notes
    def print_btn(self):
        print("Sheet music output: ")
        self.worker_thread.recording_thread = False
        self.worker_thread.filename = "Use Recording"
        self.worker_thread.note_fulllist = self.note_fulllist
        self.worker_thread.start()



    # run when thread emits signal
    def on_change(self):
        notes_2D = self.worker_thread.notes

        for notes in notes_2D:
            self.note_fulllist.append(notes)
            self.add_note(notes)

        # convert note format to helmholtz pitch notation

    # Used for rendering notes without having to create note objects.
    # This function just creates the note objects then calls add_note()
    # eg. window.add_note_by_string(['C4', 'D4', 'E4'])
    def add_note_by_string(self, new_note_string_list):
        new_note_obj_list = []

        for note_string in new_note_string_list:
            note_obj = Create_Note(note_string)
            new_note_obj_list.append(note_obj)
        self.add_note(new_note_obj_list)

    # Takes a list of note objects and renders them all in a single time slot as a chord.
    # A single note would be a list with one note object and a rest would be an empty list.
    def add_note(self, new_note_obj_list):
        # TEMP FIX TO WORK WITH STRINGS
        for note in new_note_obj_list:
            if type(note) == type(''):
                self.add_note_by_string(new_note_obj_list)
                return

        # loop through previous notes and shift them to the left
        for i in range(len(self.note_obj_list)):

            if i >= len(self.note_obj_list):
                break

            note_object = self.note_obj_list[i]
            note_label = self.note_label_list[i]

            note_object.x = note_object.x - self.DELTA_X

            note_label.move(note_object.x - self.NOTE_SIZE / 2, note_object.y - self.NOTE_SIZE / 2)
            note_label.update()
            note_label.show()

            # remove image when it reaches the border_pixel mark
            if (note_label.x() < self.BORDER_PIXEL):
                note_label.hide()
                note_label.close()
                self.note_label_list.pop(i)
                self.note_obj_list.pop(i)
                i -= 1

        # Add new notes in given list
        for new_note_obj in new_note_obj_list:
            # X position is middle of last column, where each column is DELTA_X wide
            # Y position is offset in reference to the middle C4 position
            new_note_obj.x = self.RIGHT_SIDE - self.DELTA_X / 2  # intial x value
            new_note_obj.y = self.C4_POSITION - self.DELTA_Y * self.note_delta('C4', new_note_obj.letter)

            # Create the note label and assign an image to it
            new_note_label = QtWidgets.QLabel(self)
            pixmap = QtGui.QPixmap(r'Resources\dot.png')
            pixmap = pixmap.scaled(self.NOTE_SIZE, self.NOTE_SIZE, QtCore.Qt.KeepAspectRatio)
            new_note_label.setPixmap(pixmap)

            # Update and render the new note
            new_note_label.move(new_note_obj.x - self.NOTE_SIZE / 2, new_note_obj.y - self.NOTE_SIZE / 2)
            new_note_label.update()
            new_note_label.show()

            # Store the newly created and rendered note label in note_label_list
            self.note_label_list.append(new_note_label)
            self.note_obj_list.append(new_note_obj)

            # increment note counter and update window
            Music_Window.note_index += 1
            self.update()
            self.show()

        QtTest.QTest.qWait(self.WAIT_TIME)  # wait for wait_time in ms

    def note_delta(self, note1, note2):
        '''Return the number of semitones to get from note1 to note2'''
        return librosa.note_to_midi(note2) - librosa.note_to_midi(note1)


# This thread processes all the data for the UI
class WorkerThread(QThread):
    mySignal = pyqtSignal(str)
    notes = []

    FORMAT = pyaudio.paInt16
    CHANNELS = 1
    RATE = 44100
    CHUNK = int(44100 * TIME_CHUNK)
    # Other
    NOTE_FREQUENCIES = [16.35, 17.32, 18.35, 19.45, 20.6, 21.83, 23.12, 24.5, 25.96, 27.5, 29.14, 30.87, 32.7, 34.65,
                        36.71,
                        38.89, 41.2, 43.65, 46.25, 49, 51.91, 55, 58.27, 61.74, 65.41, 69.3, 73.42, 77.78, 82.41, 87.31,
                        92.5, 98, 103.83, 110, 116.54, 123.47, 130.81, 138.59, 146.83, 155.56, 164.81, 174.61, 185, 196,
                        207.65, 220, 233.08, 246.94, 261.63, 277.18, 293.66, 311.13, 329.63, 349.23, 369.99, 392, 415.3,
                        440, 466.16, 493.88, 523.25, 554.37, 587.33, 622.25, 659.25, 698.46, 739.99, 783.99, 830.61,
                        880,
                        932.33, 987.77, 1046.5, 1108.73, 1174.66, 1244.51, 1318.51, 1396.91, 1479.98, 1567.98, 1661.22,
                        1760, 1864.66, 1975.53, 2093, 2217.46, 2349.32, 2489.02, 2637.02, 2793.83, 2959.96, 3135.96,
                        3322.44, 3520, 3729.31, 3951.07, 4186.01, 4434.92, 4698.63, 4978.03, 5274.04, 5587.65, 5919.91,
                        6271.93, 6644.88, 7040, 7458.62, 7902.13]

    # Initialize worker thread
    def __init__(self):
        super(WorkerThread, self).__init__()
        self._isRunning = True
        self.recording_thread =True;
        self.filename = ""
        self.end_of_previous = None
        self.first_chunk_flag = True
        self.note_fulllist = []

    # start of thread
    def run(self):
        print("start new thread")

        self.stream = self.init_stream()

        # file input for testing
        if not self.recording_thread:
            if(self.filename == "Use Recording"):
                note_list, dur_list = self.clean_up(self.note_fulllist)
                self.pdf_output(note_list, dur_list)
            else:
                print(self.filename)
                y, sr = librosa.load(self.filename, sr=None)
                note_list = self.get_notes(y, sr)
                output_note_list, dur_list = self.clean_up(note_list)
                self.pdf_output(output_note_list, dur_list)

        # realtime
        if self.recording_thread:
            while True:

                # The isRunning flag is set to False when the stop button is pressed
                if not self._isRunning:
                    print("isRunning == False. Breaking loop.")
                    self._isRunning = True;
                    break

                # data = self.stream.read(self.CHUNK)
                print(time.time())
                print("Starting...")
                data = self.stream.read(self.CHUNK)

                data_int = np.fromstring(data, dtype=np.int16)

                notes = self.get_notes(data_int, self.RATE)
                self.notes = notes
                print(self.notes)
                self.emit_toUI()


    def stop(self):
        self._isRunning = False

    # Call function outside of thread
    def emit_toUI(self):
        self.mySignal.emit("Random String")

    # Displays a spectrogram of D using matplotlib
    # D is the fourier coefficient matrix returned by librosa.core.stft
    def show_spectrogram(self, D):
        librosa.display.specshow(librosa.logamplitude(D ** 2), y_axis='log', x_axis='time')
        plt.title('Amplitude spectrogram')
        plt.colorbar()
        plt.tight_layout()

    # Does something
    def reference_lines(self):
        xmax = plt.xlim()[1]
        for i in self.NOTE_FREQUENCIES:
            plt.plot((0, xmax), (i, i), color='grey', linewidth='0.5')

            # -----------------------------------------------------------------------------------------------------------------------

    # Create and display spectrogram
    def make_spectrogram(self, y, sr):
        n_fft = 4000;
        D = librosa.core.stft(y, n_fft=n_fft)
        # Convert from complex numbers to magnitude
        D = abs(D)

        o_env = librosa.onset.onset_strength(y, sr=sr)
        times = librosa.frames_to_time(np.arange(len(o_env)), sr=sr)
        onset_frames = librosa.onset.onset_detect(onset_envelope=o_env, sr=sr)

        plt.figure()
        ax1 = plt.subplot(2, 1, 1)
        librosa.display.specshow(librosa.amplitude_to_db(D, ref=np.max),
                                 x_axis='time', y_axis='log')
        plt.title('Power spectrogram')
        plt.subplot(2, 1, 2, sharex=ax1)
        plt.plot(times, o_env, label='Onset strength')
        plt.vlines(times[onset_frames], 0, o_env.max(), color='r', alpha=0.9,
                   linestyle='--', label='Onsets')
        plt.axis('tight')
        plt.legend(frameon=True, framealpha=0.75)

        if 0:
            # Set magnitude to either 1 or 0 based on whether it's above or below the threshold
            threshold = D.mean() + (D.mean() + D.max()) / 2
            D[D < threshold] = 0
            D[D > threshold] = 1

            plt.figure()
            librosa.display.specshow(librosa.logamplitude(D ** 2), y_axis='log', x_axis='time')
            plt.title('Amplitude spectrogram')
            plt.colorbar()
            plt.tight_layout()
            self.reference_lines()

            # -----------------------------------------------------------------------------------------------------------------------

    # get_notes

    # stream audio
    def init_stream(self):
        # initialize stream parameters
        p = pyaudio.PyAudio()
        stream = p.open(format=self.FORMAT,
                        channels=self.CHANNELS,
                        rate=self.RATE,
                        input=True,
                        frames_per_buffer=self.CHUNK)
        return stream

    # Return list of sampled notes
    def get_notes(self, y, sr):

        # Returns 2D np.ndarray complex FT coefficients
        # np.abs(D[f, t]) is the magnitude of frequency bin f at frame t
        n_fft = 4000

        # ie. Samples per frame =  (Samples per second) / (frames per second)
        hop_length = int(44100 / DOTS_PER_SECOND)

        # If y has N samples we expect D to have N/hop_length + 1 time frames
        # D will have 1 + n_fft/2 frequency levels
        D = librosa.core.stft(y, n_fft=n_fft, hop_length=hop_length)

        ##### WORK IN PROGRESS. REMOVING HARMONICS
        # D_oct = np.copy(D)
        # Shift D up an octave
        # for f in range(D.shape[0]):
        #     for t in range(D.shape[1]):
        #         try:
        #             D_oct[f,t] = D[f/2,t]
        #         except IndexError:
        #             pass
        ##### WORK IN PROGRESS. REMOVING HARMONICS

        #### padding
        padding = 4 * hop_length

        if self.first_chunk_flag:  # First chunk
            self.first_chunk_flag = False
        else:
            y_pad = np.concatenate([self.end_of_previous, y])
            D_pad = librosa.core.stft(y_pad, n_fft=n_fft, hop_length=hop_length)

            # The resulting excess frames on D due to y padding
            padding_D = int(padding / hop_length)

            # Trim the extra frames
            D_pad = D_pad[:, padding_D:-padding_D]

            D_initial = D
            D = D_pad

        self.end_of_previous = y[-padding:]

        # Convert from complex numbers to magnitude
        D = abs(D)

        # Set magnitude to either 1 or 0 based on whether it's above or below the threshold
        threshold = np.percentile(D, 99.60)
        D[D < threshold] = 0
        D[D > threshold] = 1

        # Create a list of empty lists to hold the notes in each sample
        frequencies = [[] for _ in range(D.shape[1])]
        notes = [[] for _ in range(D.shape[1])]

        # Iterate through the samples and frequency index values of D and find 1s, then add to frequencies list
        for t in range(D.shape[1]):
            for f in range(D.shape[0]):
                if D[f, t] == 1:
                    # Convert the STFT frequency index to a frequency Hz value
                    freq = librosa.core.fft_frequencies(sr=sr, n_fft=n_fft)[f]

                    # Append the frequency to the sublist
                    frequencies[t].append(freq)

        # Average adjacent frequencies
        for t in range(len(frequencies)):

            frame_freqs = frequencies[t]

            # If the frame has one or no frequencies, skip to the next frame
            if len(frame_freqs) < 2:
                continue

            # This will hold the frequencies in the frame after averaging together adjacent frequencies
            # and will replace frame_freqs in frequencies[t]
            new_frame_freqs = []

            # Get the delta between frequencies for the frequency resolution used
            temp = librosa.core.fft_frequencies(sr=sr, n_fft=n_fft)
            delta = temp[1] - temp[0] + 1  # +1 as an epsilon term

            # This holds current chunk of adjacent frequencies in the frame
            # Initialize it with the first frequency of the frame
            adjacent_freqs = []
            # Iterate through the rest of the frequencies in the frame and add to list if adjacent
            for i in range(len(frame_freqs)):
                try:
                    # Add the first freq encountered to the list
                    if adjacent_freqs == []:
                        adjacent_freqs.append(frame_freqs[i])

                    if frame_freqs[i + 1] - frame_freqs[i] < delta:
                        adjacent_freqs.append(frame_freqs[i + 1])
                    else:
                        # Reached the end of this chunk of adjacent notes. Average and add it to frame freq list
                        if not adjacent_freqs == []:
                            average_freq = np.average(adjacent_freqs)
                            new_frame_freqs.append(average_freq)
                            adjacent_freqs = []
                except IndexError:
                    average_freq = np.average(adjacent_freqs)
                    new_frame_freqs.append(average_freq)

            frequencies[t] = new_frame_freqs

        # Convert frequencies to notes
        for samp in range(len(frequencies)):
            # Remove the zeros, otherwise hz_to_note throws an error
            while 0 in frequencies[samp]:
                frequencies[samp].remove(0)
            # This function takes a list of frequencies and returns a list of notes
            notes[samp] = librosa.core.hz_to_note(frequencies[samp])

        notes = self.denoise(notes, 6)
        print(frequencies)
        return notes

    def denoise(self, notes, min_group_size):
        # Remove noise (small clusters of notes)
        orig_notes = copy.deepcopy(notes)
        min_group_size = min_group_size
        for i in range(len(orig_notes)):  # Iterates over frame indexes
            for note in orig_notes[i]:
                groupsize = 1

                # Check ahead for notes
                for j in range(1, min_group_size):
                    if i + j >= len(orig_notes):
                        break
                    elif note in orig_notes[i + j]:
                        groupsize += 1
                    else:
                        break

                # Check behind for notes
                for j in range(1, min_group_size):
                    if i - j < 0:
                        break
                    elif note in orig_notes[i - j]:
                        groupsize += 1
                    else:
                        break

                if groupsize < min_group_size:
                    notes[i].remove(note)

        return notes
    # detect note duration and create list of chords
    def clean_up(self, note_list):
        # note_list = [['C4'], ['C4'], ['C4'], ['C4'], ['D4'], ['D4'], ['D4'], ['D4'], ['D4'],[],[],[],[],[],
        #  ['E4'], ['E4'], ['E4'], ['E4'], ['E4'], ['F4'], ['F4'], ['F4'], ['F4'], ['F4'], ['G4'], ['G4'], ['G4'],
        #  ['G4'], ['G4'], ['C5', 'D5', 'E5', 'F5'],['E5', 'D5', 'C5', 'F5'],['C5', 'D5', 'E5', 'F5'],['C5', 'D5', 'E5', 'F5'],['C5', 'D5', 'E5', 'F5'],['C5', 'D5', 'E5', 'F5'],['C5', 'D5', 'E5', 'F5'],['C5', 'D5', 'E5', 'F5'], ['G4'], ['G4'], ['G4']]
        # counter variable
        i = 0
        # list of notes
        output_list = []
        # list of durations for each note
        note_duration_list = []
        note_dur_count = 0

        #for emppty case
        for i in range(len(note_list)):
            if not (note_list[i]):
                note_list[i] = ['R']

        for i in range(len(note_list)):
            for x in note_list[i]:

                if i == len(note_list) - 1:
                    temp = float(note_dur_count) / float(len(note_list[i])) + 1.0
                    # print(i)
                    # print(temp)
                    if (temp > 1):
                        output_list.append(note_list[i])
                        note_duration_list.append(float(temp))
                        # print(temp)
                    break;

                if x in note_list[i + 1]:
                    note_dur_count += 1

                else:
                    temp = float(note_dur_count) / float(len(note_list[i])) + 1.0
                    if (temp > 1):
                        output_list.append(note_list[i])
                        note_duration_list.append(float(temp))
                    note_dur_count = 0

        print(*note_duration_list, sep=", ")
        string_duration_list = []
        for y in range(len(note_duration_list)):
            if (note_duration_list[y] <= 4):
                string_duration_list.append("16")
            if (note_duration_list[y] > 4 and note_duration_list[y] <= 8):
                string_duration_list.append("8")
            if (note_duration_list[y] > 8 and note_duration_list[y] <= 16):
                string_duration_list.append("4")
            if (note_duration_list[y] > 16):
                string_duration_list.append("2")
        print(*output_list, sep=", ")
        print(*string_duration_list, sep=", ")

        return output_list, string_duration_list

    # output to pdf
    def pdf_output(self, music_note_list, note_duration_list):
        # list for pdf output (lilypond)

        # ---------------------TESTING DATA ---------------------------------------------------------
        #music_note_list = [['D#4'], ['C4'], ['G4'], ['G#4'],['D#4'], ['G#3'], ['A#3'],['D#4'],['C4'], ['F4'],['D#4'],['C#4'],['C4'],['A#3']]
        #music_note_list = [['D#5'], ['C5'], ['G5'],['D#5'], ['G#4'], ['G#4'] ]

        #note_duration_list = ['4','4', '4', '4','2', '4', '4','4','4', '2','4','4','4','4']
            #['4', '8', '16','4', '4', '4', '8','8','16']

        # chord_test = Chord("<c'' f'' d''>4")
        # chord_test.written_duration = Duration(1, 16)
        # rest_test = Rest(Duration(1, 4))
        # output_list = [Note("c''4"), Note("d''8"), Note("e''16"), Note("f''32"), rest_test, chord_test, Note("d''4"),Rest('r8'), Note("e''4"),Chord([4, 6, 14], Duration(1, 4)), Note("f''4")]
        # container = Container(output_list)
        # show(container)
        #------------------------------------------------------------------------------

        output_list = []
        chord_check =0
        # loop through each note and convert it to the correct format and duration
        for i in range(len(music_note_list)):
            temp_chord = ""
            chord_check = 0
            # loop through notes being played at the same time and add them together as a chord
            for j in range(len(music_note_list[i])):
                chord_check +=1
                # each notes format is converted
                if(music_note_list[i][j] != 'R'):
                    temp_chord += " "
                    temp_chord += self.convert_format(music_note_list[i][j])
                #print(temp_chord)
            # if empty list = rest
            if music_note_list[i][j] == 'R':
                # add a rest to output
                note = Rest('r'+note_duration_list[i])
            # otherwise add chords together
            else:
                # add a chored to output
                note =Chord("<" + temp_chord + ">" + note_duration_list[i])

            # add chord/note/rest to outputlist
            output_list.append(note)
        # Output list to pdf
        container = Container(output_list)
        show(container)

    def convert_format(self, music_note):

            letter_note = music_note[0]  # Letter of the note
            sharp_or_octave = music_note[1]  # sharp or octave
            octave_check = ''
            if (len(music_note) >= 3):
                octave_check = music_note[2]  # octave or nothing
            octave = "'"

            # Set to lowercase letters with sharps
            if letter_note == "A" and sharp_or_octave == "#":
                letter_note = "as"
            if letter_note == "B" and sharp_or_octave == "#":
                letter_note = "bs"
            if letter_note == "C" and sharp_or_octave == "#":
                letter_note = "cs"
            if letter_note == "D" and sharp_or_octave == "#":
                letter_note = "ds"
            if letter_note == "E" and sharp_or_octave == "#":
                letter_note = "es"
            if letter_note == "F" and sharp_or_octave == "#":
                letter_note = "fs"
            if letter_note == "G" and sharp_or_octave == "#":
                letter_note = "gs"

            # Set to lowercase letters
            letter_note = letter_note.lower()

            # Set Octave when sharp present
            if (sharp_or_octave == "#" and octave_check == "0"):
                octave = ",,,"
            if (sharp_or_octave == "#" and octave_check == "1"):
                octave = ",,"
            if (sharp_or_octave == "#" and octave_check == "2"):
                octave = ","
            if (sharp_or_octave == "#" and octave_check == "3"):
                octave = "'"
            if (sharp_or_octave == "#" and octave_check == "4"):
                octave = "'"
            if (sharp_or_octave == "#" and octave_check == "5"):
                octave = "''"
            if (sharp_or_octave == "#" and octave_check == "6"):
                octave = "'''"
            if (sharp_or_octave == "#" and octave_check == "7"):
                octave = "''''"
            if (sharp_or_octave == "#" and octave_check == "8"):
                octave = "'''''"
            if (sharp_or_octave == "#" and octave_check == "9"):
                octave = "''''''"

            # Set Octave when sharp not present
            if (sharp_or_octave == "0"):
                octave = ",,,"
            if (sharp_or_octave == "1"):
                octave = ",,"
            if (sharp_or_octave == "2"):
                octave = ","
            if (sharp_or_octave == "3"):
                octave = "'"
            if (sharp_or_octave == "4"):
                octave = "'"
            if (sharp_or_octave == "5"):
                octave = "''"
            if (sharp_or_octave == "6"):
                octave = "'''"
            if (sharp_or_octave == "7"):
                octave = "''''"
            if (sharp_or_octave == "8"):
                octave = "'''''"
            if (sharp_or_octave == "9"):
                octave = "''''''"

            # return note with helmholtz pitch notation
            music_note = letter_note + octave
            # print("convert_format5")
            return music_note




if __name__ == '__main__':

    # enable PyQt traceback
    def my_excepthook(type, value, tback):
        # log the exception here
        # then call the default handler
        sys.__excepthook__(type, value, tback)


    import sys

    sys.excepthook = my_excepthook

    # Test realtime
    if 1:
        app = QApplication(sys.argv)
        # global Window#???
        window = Music_Window()
        #window.add_note(['C4', 'C5'])

    # Test get_notes
    if 0:
        # audio_path = r"Resources\A4-C5-E5_16bit_int.wav"
        audio_path = r"Resources\C4_sine.wav"
        app = QApplication(sys.argv)
        # global Window#???
        window = Music_Window()
        T = WorkerThread()

        # y = 1D numpy array of audio samples
        # sr = sampling rate. Set to None to use native SR (44100 for wav)
        y, sr = librosa.load(audio_path, sr=None)

        for sub_y in np.array_split(y, 3):

            n2d = T.get_notes(sub_y, sr)
            for notes in n2d:
                window.add_note(notes)


    sys.exit(app.exec_())  # not needed??
    plt.show()