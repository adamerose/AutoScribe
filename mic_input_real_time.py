import pyaudio
import threading
import queue
import time
import numpy as np

def MicInput(FORMAT, CHANNELS, RATE, CHUNK, q):

    # create userstop thread
    class myThread (threading.Thread):
        def __init__(self):
            threading.Thread.__init__(self)
        def run(self):
            input("Press Enter to stop:\n ")

    audio = pyaudio.PyAudio()

    # start recording stream
    inputstream = audio.open(format=FORMAT, channels=CHANNELS,
                        rate=RATE, input=True,
                        frames_per_buffer=CHUNK)
    print("recording start")

    # prompt user stop input
    userstop = myThread()
    userstop.start()

    # start record
    while userstop.is_alive():
        data = inputstream.read(CHUNK)
        signal = np.frombuffer(data, dtype='<i2')
        q.put(np.ndarray.astype(signal, dtype=np.float32))

    # stop recording
    inputstream.stop_stream()
    inputstream.close()
    audio.terminate()

def MicConversion(FORMAT, CHANNELS, RATE, CHUNK, q):

    # wait for mic to gather data
    time.sleep(.05)

    # print converted mic input
    while MicInputthread.is_alive():
        data = q.get()
        print(data)

# initialize stream parameters
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 44100
CHUNK = 1024

# start mic record and playback
q=queue.Queue()
MicInputthread = threading.Thread(target=MicInput, args=(FORMAT, CHANNELS, RATE, CHUNK, q))
MicConversionthread = threading.Thread(target=MicConversion, args=(FORMAT, CHANNELS, RATE, CHUNK, q))
MicInputthread.start()
MicConversionthread.start()

# wait for recording to stop
while MicInputthread.is_alive() & MicConversionthread.is_alive():
    time.sleep(2)
    print('recording...')

print("finished recording")
