import sys
from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.QtWidgets import *
from PyQt5 import QtTest
import time
import threading

# Note Object
class Create_Note():
    def __init__(self, letter, start_location):
        # Create a note object and set its parameters
        self.letter = letter
        self.start_location = start_location
        self.x = 0
        self.y = 0

# Window Object
class Music_Window(QWidget):  # QWidget
    # Define Class Variables
    note_id = 0  # Index for array of notes/lables
    destroy = 0

    def __init__(self):
        super(Music_Window, self).__init__()
        self.title = "Sheet Music Display"  # Title
        self.initUI()

    # Configure the default settings of the MusicWindow object
    def initUI(self):
        # Window Settings
        self.setWindowTitle(self.title)  # Title
        self.setWindowIcon(QtGui.QIcon(r"Resources\logo.ico"))  # Icon
        self.setGeometry(100, 100, 1340, 650)  # Windows size

        # Display a brackground image
        sheet_image = QtWidgets.QLabel(self)
        sheet_image.setPixmap(QtGui.QPixmap(r'Resources\blank_sheet.png'))

        # Array of note labels and objects
        self.note_label_list = {}
        self.note_obj_list = {}

        button = QPushButton('Finish Recording', self)
        button.setToolTip('This is button will close the window and output the sheet music to a pdf')
        button.move(30, 400)
        button.clicked.connect(self.on_click)

        self.show()

    def on_click(self):
        self.destroy = 1
        self.hide()
        print('Close Realtime Window')

    # Add a new label to the MusicWindow with the image and location that corresponds to the provided note object
    def add_note(self, new_note):
        if (self.destroy == 0):
            middlC_pixel = 69  # middle C pixel location
            delta_pixel = 5.7  # pixels between notes
            new_note.x = 1300  # intial x value
            new_note.y = 30  # intial y value
            wait_time = 500  # wait time in ms
            border_pixel = 30  # note boundary delete label at this pixel value

            # check if the sample is a rest
            if (new_note.letter == "rest"):
                self.note_label_list[Music_Window.note_id] = QtWidgets.QLabel(self)

                # Store the note object in the note_obj_list.
                self.note_obj_list[Music_Window.note_id] = new_note
                self.note_obj_list[Music_Window.note_id].x = new_note.x
                self.note_obj_list[Music_Window.note_id].y = new_note.y

                # loop through all notes and shift them to the left 10 pixels
                for i in self.note_label_list:
                    self.note_obj_list[i].x -= 10
                    self.note_label_list[i].move((self.note_obj_list[i].x), (self.note_obj_list[i].y))
                    self.note_label_list[i].update()
                    self.note_label_list[i].show()

                    # remove image when it reaches the border_pixel mark
                    if (self.note_obj_list[i].x < border_pixel):
                        self.note_obj_list[i].x.hide()

                # increment note counter and update window
                Music_Window.note_id += 1
                self.update()
                self.show()
                QtTest.QTest.qWait(wait_time)  # wait for wait_time in ms
            else:
                # Determine Y Location of the note
                if (new_note.letter == "a'" or new_note.letter == "as'"):
                    new_note.y = middlC_pixel + 2 * delta_pixel
                if (new_note.letter == "b'" or new_note.letter == "bs'"):
                    new_note.y = middlC_pixel + delta_pixel
                    # middle C set to 69
                if (new_note.letter == "c'" or new_note.letter == "cs'"):
                    new_note.y = middlC_pixel
                if (new_note.letter == "d'" or new_note.letter == "ds'"):
                    new_note.y = middlC_pixel - 1 * delta_pixel
                if (new_note.letter == "e'" or new_note.letter == "es'"):
                    new_note.y = middlC_pixel - 2 * delta_pixel
                if (new_note.letter == "f'" or new_note.letter == "fs'"):
                    new_note.y = middlC_pixel - 3 * delta_pixel
                if (new_note.letter == "g'" or new_note.letter == "gs'"):
                    new_note.y = middlC_pixel - 4 * delta_pixel
                if (new_note.letter == "a''" or new_note.letter == "as''"):
                    new_note.y = 2
                if (new_note.letter == "b''" or new_note.letter == "bs''"):
                    new_note.y = -3.5
                if (new_note.letter == "c''" or new_note.letter == "cs''"):
                    new_note.y = middlC_pixel - 7 * delta_pixel
                if (new_note.letter == "d''" or new_note.letter == "ds''"):
                    new_note.y = middlC_pixel - 8 * delta_pixel
                if (new_note.letter == "e''" or new_note.letter == "es''"):
                    new_note.y = middlC_pixel - 9 * delta_pixel
                if (new_note.letter == "f''" or new_note.letter == "fs''"):
                    new_note.y = middlC_pixel - 10 * delta_pixel
                if (new_note.letter == "g''" or new_note.letter == "gs''"):
                    new_note.y = middlC_pixel - 11 * delta_pixel
                if (new_note.letter == "a'''" or new_note.letter == "as'''"):
                    new_note.y = middlC_pixel - 12 * delta_pixel
                if (new_note.letter == "b'''" or new_note.letter == "bs'''"):
                    new_note.y = middlC_pixel - 13 * delta_pixel
                if (new_note.letter == "c'''" or new_note.letter == "cs'''"):
                    new_note.y = middlC_pixel - 14 * delta_pixel

                # Create the note label and store it in note_label_list
                self.note_label_list[Music_Window.note_id] = QtWidgets.QLabel(self)
                # select the image to display
                pixmap = QtGui.QPixmap(r'Resources\dot.png')
                pixmaptemp = pixmap.scaled(80, 80, QtCore.Qt.KeepAspectRatio)  # resize image
                self.note_label_list[Music_Window.note_id].setPixmap(pixmaptemp)

                # Store the note object in the note_obj_list array.
                self.note_obj_list[Music_Window.note_id] = new_note
                self.note_obj_list[Music_Window.note_id].x = new_note.x
                self.note_obj_list[Music_Window.note_id].y = new_note.y

                # loop through all notes and shift them to the left 10 pixels
                for i in self.note_label_list:
                    self.note_obj_list[i].x -= 10
                    self.note_label_list[i].move((self.note_obj_list[i].x), (self.note_obj_list[i].y))
                    self.note_label_list[i].update()
                    self.note_label_list[i].show()

                    # remove image when it reaches the border_pixel mark
                    if (self.note_obj_list[i].x <= border_pixel):
                        self.note_label_list[i].hide()

                # increment note counter and update window
                Music_Window.note_id += 1
                self.update()
                self.show()
                QtTest.QTest.qWait(wait_time)  # wait for wait_time in ms
