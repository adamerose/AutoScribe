import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import matplotlib.style as ms
import librosa
import librosa.display
import pyaudio
import time
import sys
from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.QtWidgets import *
from abjad import *
import abjad
import music_window
import realtime_music_window

# Global Settings
# PyAudio

BPM = 120
BPS = BPM / 60
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 44100
CHUNK = 44100 * 5
# Other
NOTE_FREQUENCIES = [16.35, 17.32, 18.35, 19.45, 20.6, 21.83, 23.12, 24.5, 25.96, 27.5, 29.14, 30.87, 32.7, 34.65, 36.71,
                    38.89, 41.2, 43.65, 46.25, 49, 51.91, 55, 58.27, 61.74, 65.41, 69.3, 73.42, 77.78, 82.41, 87.31,
                    92.5, 98, 103.83, 110, 116.54, 123.47, 130.81, 138.59, 146.83, 155.56, 164.81, 174.61, 185, 196,
                    207.65, 220, 233.08, 246.94, 261.63, 277.18, 293.66, 311.13, 329.63, 349.23, 369.99, 392, 415.3,
                    440, 466.16, 493.88, 523.25, 554.37, 587.33, 622.25, 659.25, 698.46, 739.99, 783.99, 830.61, 880,
                    932.33, 987.77, 1046.5, 1108.73, 1174.66, 1244.51, 1318.51, 1396.91, 1479.98, 1567.98, 1661.22,
                    1760, 1864.66, 1975.53, 2093, 2217.46, 2349.32, 2489.02, 2637.02, 2793.83, 2959.96, 3135.96,
                    3322.44, 3520, 3729.31, 3951.07, 4186.01, 4434.92, 4698.63, 4978.03, 5274.04, 5587.65, 5919.91,
                    6271.93, 6644.88, 7040, 7458.62, 7902.13]


# Displays a spectrogram of D using matplotlib
# D is the fourier coefficient matrix returned by librosa.core.stft
def show_spectrogram(D):
    librosa.display.specshow(librosa.logamplitude(D ** 2), y_axis='log', x_axis='time')
    plt.title('Amplitude spectrogram')
    plt.colorbar()
    plt.tight_layout()


def reference_lines():
    xmax = plt.xlim()[1]
    for i in NOTE_FREQUENCIES:
        plt.plot((0, xmax), (i, i), color='grey', linewidth='0.5')


def make_spectrogram(y, sr):
    n_fft = 4000;
    D = librosa.core.stft(y, n_fft=n_fft)
    # Convert from complex numbers to magnitude
    D = abs(D)

    o_env = librosa.onset.onset_strength(y, sr=sr)
    times = librosa.frames_to_time(np.arange(len(o_env)), sr=sr)
    onset_frames = librosa.onset.onset_detect(onset_envelope=o_env, sr=sr)

    plt.figure()
    ax1 = plt.subplot(2, 1, 1)
    librosa.display.specshow(librosa.amplitude_to_db(D, ref=np.max),
                             x_axis='time', y_axis='log')
    plt.title('Power spectrogram')
    plt.subplot(2, 1, 2, sharex=ax1)
    plt.plot(times, o_env, label='Onset strength')
    plt.vlines(times[onset_frames], 0, o_env.max(), color='r', alpha=0.9,
               linestyle='--', label='Onsets')
    plt.axis('tight')
    plt.legend(frameon=True, framealpha=0.75)

    if 0:
        # Set magnitude to either 1 or 0 based on whether it's above or below the threshold
        threshold = D.mean() + (D.mean() + D.max()) / 2
        D[D < threshold] = 0
        D[D > threshold] = 1

        plt.figure()
        librosa.display.specshow(librosa.logamplitude(D ** 2), y_axis='log', x_axis='time')
        plt.title('Amplitude spectrogram')
        plt.colorbar()
        plt.tight_layout()
        reference_lines()


def get_notes(y, sr):
    # Returns 2D np.ndarray complex FT coefficients
    # np.abs(D[f, t]) is the magnitude of frequency bin f at frame t
    n_fft = 4000;
    D = librosa.core.stft(y, n_fft=n_fft)

    # Get onset times
    o_env = librosa.onset.onset_strength(y, sr=sr)
    onset_frames = librosa.onset.onset_detect(onset_envelope=o_env, sr=sr)

    # Convert from complex numbers to magnitude
    D = abs(D)

    # Set magnitude to either 1 or 0 based on whether it's above or below the threshold
    threshold = D.mean() + (D.mean() + D.max()) / 2
    D[D < threshold] = 0
    D[D > threshold] = 1

    # Return a 1D array of f,t values where D==1
    f, t = np.where(D == 1)

    # Create a list of empty lists to hold the notes in each timeslot
    frequencies = [[] for _ in range(D.shape[1])]
    # Convert f from index to Hz
    for t in range(D.shape[1]):
        for f in range(D.shape[0]):
            if D[f, t] == 1:
                frequencies[t].append(f);

                # f[i] = librosa.core.fft_frequencies(sr=sr, n_fft=n_fft)[f[i]]

    # Convert frequency STFT indexes to Hz
    for t in range(len(frequencies)):
        for i in range(len(frequencies[t])):
            frequencies[t][i] = librosa.core.fft_frequencies(sr=sr, n_fft=n_fft)[frequencies[t][i]]

    # Convert frequencies to notes
    notes = []
    for t in range(len(frequencies)):
        freq = None
        if len(frequencies[t]) > 0:
            freq = np.mean(frequencies[t])
        notes.append(librosa.core.hz_to_note(freq))

    return notes


def init_stream():
    # initialize stream parameters

    p = pyaudio.PyAudio()
    stream = p.open(format=FORMAT,
                    channels=CHANNELS,
                    rate=RATE,
                    input=True,
                    frames_per_buffer=CHUNK)

    return stream


def output_notes(OutputWindow, notes):
    # counter variable
    i = 0
    # list for python display
    ouput_notes = []
    # list for pdf output (lilypond)
    output_list = []
    # Set to 1 to remove transistion notes 0 to keep transistion notes
    transition_flag = 1
    # count the number of samples for the same note
    note_dur_count = 0
    dur_flag = 0
    global realtime_flag
    #realtime_flag = 1
    # Create python window
    # app = QApplication(sys.argv)
    # OutputWindow = MusicWindow.Music_Window()
    for item in notes:
        flag = 0  # flag set to 1 if transistion note
        letter_note = str(item)[2]  # Letter of the note
        sharp_or_octave = str(item)[3]  # sharp or octave
        octave_check = str(item)[4]  # octave or nothing

        # Set to lowercase letters with sharps
        if letter_note == "A" and sharp_or_octave == "#":
            letter_note = "as"
        if letter_note == "B" and sharp_or_octave == "#":
            letter_note = "bs"
        if letter_note == "C" and sharp_or_octave == "#":
            letter_note = "cs"
        if letter_note == "D" and sharp_or_octave == "#":
            letter_note = "ds"
        if letter_note == "E" and sharp_or_octave == "#":
            letter_note = "es"
        if letter_note == "F" and sharp_or_octave == "#":
            letter_note = "fs"
        if letter_note == "G" and sharp_or_octave == "#":
            letter_note = "gs"

        # Set to lowercase letters
        letter_note = letter_note.lower()

        # Set Octave when sharp present
        if (sharp_or_octave == "#" and octave_check == "0"):
            octave = ",,"
        if (sharp_or_octave == "#" and octave_check == "1"):
            octave = ","
        if (sharp_or_octave == "#" and octave_check == "2"):
            octave = ""
        if (sharp_or_octave == "#" and octave_check == "3"):
            octave = "'"
        if (sharp_or_octave == "#" and octave_check == "4"):
            octave = "''"
        if (sharp_or_octave == "#" and octave_check == "5"):
            octave = "'''"
        if (sharp_or_octave == "#" and octave_check == "6"):
            octave = "''''"
        if (sharp_or_octave == "#" and octave_check == "7"):
            octave = "'''''"

        # Set Octave when sharp not present
        if (sharp_or_octave == "0"):
            octave = ",,"
        if (sharp_or_octave == "1"):
            octave = ","
        if (sharp_or_octave == "2"):
            octave = ""
        if (sharp_or_octave == "3"):
            octave = "'"
        if (sharp_or_octave == "4"):
            octave = "''"
        if (sharp_or_octave == "5"):
            octave = "'''"
        if (sharp_or_octave == "6"):
            octave = "''''"
        if (sharp_or_octave == "7"):
            octave = "'''''"

        # count the number of samples of the same note
        if (notes[i] == notes[i - 1] and dur_flag == 1):
            note_dur_count += 1
        else:
            note_dur_count = 0
        # set note duration based on number of the same note sampled
        if (note_dur_count < 8):
            note_duration = "16"
        if (note_dur_count > 8 and note_dur_count < 16):
            note_duration = "8"
        else:
            note_duration = "4"
        # list notes in order not proper spaceing
        global COUNT

        # append note with helmholtz pitch notation
        ouput_notes.append(letter_note + octave + note_duration)

        # Set flag when trasistion to new note
        if (i < len(notes) - 1 and i > 0 and transition_flag):
            if (notes[i - 1] != notes[i] and notes[i] != notes[i + 1]):
                flag = 1
        # realtime
        if (realtime_flag and flag == 0):
            COUNT += 1
            note = realtime_music_window.Create_Note(letter_note + octave, COUNT, int(note_duration))
            OutputWindow.add_note(note)
            # lillypond
            output_list.append(Note(ouput_notes[i]))
        # full wav file
        if (i < len(notes) - 1 and i > 0 and transition_flag and realtime_flag == 0):
            # display note
            if ((i == 0 or notes[i] != notes[i + 1] or i == len(notes) - 2) and flag == 0):
                COUNT += 1

                # Python Display
                note = music_window.Create_Note(letter_note + octave, COUNT, int(note_duration))
                print(letter_note + octave)
                OutputWindow.add_note(note)

                # lillypond
                output_list.append(Note(ouput_notes[i]))
        i += 1

    # Output to pdf
    if(realtime_flag==0):
        #print(output_list)
        container = Container(output_list)
        show(container)

    return output_list


COUNT = 0
realtime_flag=1

def main():
    global realtime_flag
    app = QApplication(sys.argv)
    Window = music_window.Music_Window()

    if realtime_flag:
        Window = realtime_music_window.Music_Window()


    # file input
    if 1:
        # audio_path = r"Resources\A4-C5-E5_16bit_int.wav"
        audio_path = r"Resources\pattern2_sine.wav"
        # audio_path =  r"Resources\pattern2_piano.wav"
        # y = 1D numpy array of audio samples
        # sr = sampling rate. Set to None to use native SR (44100 for wav)
        y, sr = librosa.load(audio_path, sr=None)
        notes = get_notes(y, sr)
        output = output_notes(Window, notes)
        QWidget.setFocus(Window)

        audio_path = r"Resources\pattern2_sine.wav"
        y, sr = librosa.load(audio_path, sr=None)
        notes = get_notes(y, sr)
        output = output_notes(Window, notes)

        audio_path = r"Resources\pattern2_sine.wav"
        y, sr = librosa.load(audio_path, sr=None)
        notes = get_notes(y, sr)
        output = output_notes(Window, notes)

        print(notes)
        # make_spectrogram(y, sr)

    # realtime
    if 0:
        stream = init_stream()
        full_data = np.array([], dtype=np.int16)

        while True:
            data = stream.read(CHUNK)
            print(time.time())
            print("Starting...")
            data = stream.read(CHUNK)

            data_int = np.fromstring(data, dtype=np.int16)
            full_data = np.append(full_data, data_int)
            make_spectrogram(data_int, RATE)

            plt.pause(5)
            input()
            time.sleep(0.5)
        notes = get_notes(data_int, RATE)
        output = output_notes(Window, notes)
        plt.close()
    sys.exit(app.exec_())
    pass


if __name__ == '__main__':
    main()
