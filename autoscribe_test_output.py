import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
import matplotlib.style as ms
import librosa
import librosa.display
from abjad import*
import abjad


NOTE_FREQUENCIES = [16.35,17.32,18.35,19.45,20.6,21.83,23.12,24.5,25.96,27.5,29.14,30.87,32.7,34.65,36.71,38.89,41.2,43.65,46.25,49,51.91,55,58.27,61.74,65.41,69.3,73.42,77.78,82.41,87.31,92.5,98,103.83,110,116.54,123.47,130.81,138.59,146.83,155.56,164.81,174.61,185,196,207.65,220,233.08,246.94,261.63,277.18,293.66,311.13,329.63,349.23,369.99,392,415.3,440,466.16,493.88,523.25,554.37,587.33,622.25,659.25,698.46,739.99,783.99,830.61,880,932.33,987.77,1046.5,1108.73,1174.66,1244.51,1318.51,1396.91,1479.98,1567.98,1661.22,1760,1864.66,1975.53,2093,2217.46,2349.32,2489.02,2637.02,2793.83,2959.96,3135.96,3322.44,3520,3729.31,3951.07,4186.01,4434.92,4698.63,4978.03,5274.04,5587.65,5919.91,6271.93,6644.88,7040,7458.62,7902.13]

# audio_path = r"Resources\A4-C5-E5_16bit_int.wav"
audio_path = r"Resources\pattern2_sine.wav"


# y = 1D numpy array of audio samples
# sr = sampling rate. Set to None to use native SR (44100 for wav)
y, sr = librosa.load(audio_path, sr=None)

# Displays a spectrogram of D using matplotlib
# D is the fourier coefficient matrix returned by librosa.core.stft
def show_spectrogram(D):
    librosa.display.specshow(librosa.logamplitude(D**2), y_axis='log', x_axis='time')
    plt.title('Amplitude spectrogram')
    plt.colorbar()
    plt.tight_layout()

def reference_lines():
    xmax = plt.xlim()[1]
    for i in NOTE_FREQUENCIES:
        plt.plot((0, xmax), (i, i), color='grey', linewidth='0.5')

# Returns 2D np.ndarray complex FT coefficients
# np.abs(D[f, t]) is the magnitude of frequency bin f at frame t
n_fft = 4000;
D = librosa.core.stft(y, n_fft = n_fft)

# Convert from complex numbers to magnitude
D = abs(D)

plt.figure()
librosa.display.specshow(librosa.logamplitude(D**2), y_axis='log', x_axis='time')
plt.title('Amplitude spectrogram')
plt.colorbar()
plt.tight_layout()
reference_lines()

# Set magnitude to either 1 or 0 based on whether it's above or below the threshold
threshold = D.mean() + (D.mean() + D.max()) / 2
D[D<threshold] = 0
D[D>threshold] = 1

plt.figure()
librosa.display.specshow(librosa.logamplitude(D**2), y_axis='log', x_axis='time')
plt.title('Amplitude spectrogram')
plt.colorbar()
plt.tight_layout()
reference_lines()

# Return a 1D array of f,t values where D==1
f,t = np.where(D==1)

# Create a list of empty lists to hold the notes in each timeslot
frequencies = [ [] for _ in range(D.shape[1]) ]
# Convert f from index to Hz
for t in range(D.shape[1]):
    for f in range(D.shape[0]):
        if D[f,t] == 1:
            frequencies[t].append(f);

    #f[i] = librosa.core.fft_frequencies(sr=sr, n_fft=n_fft)[f[i]]

# Convert frequency STFT indexes to Hz
for t in range(len(frequencies)):
    for i in range(len(frequencies[t])):
        frequencies[t][i] = librosa.core.fft_frequencies(sr=sr, n_fft=n_fft)[frequencies[t][i]]

# Convert frequencies to notes
notes = []
for t in range(len(frequencies)):
    freq = None
    if len(frequencies[t]) > 0:
        freq = np.mean(frequencies[t])
        letter_note=str(librosa.core.hz_to_note(freq))[2] #Letter of the note
        sharp_or_octave = str(librosa.core.hz_to_note(freq))[3] #sharp or octave
        octave_check = str(librosa.core.hz_to_note(freq))[4] #octave or nothing

    # Set to lowercase letters with sharps
    if letter_note == "A" and sharp_or_octave =="#":
        letter_note="as"
    if letter_note == "B" and sharp_or_octave =="#":
        letter_note="bs"
    if letter_note == "C" and sharp_or_octave =="#":
        letter_note="cs"
    if letter_note == "D" and sharp_or_octave == "#":
        letter_note = "ds"
    if letter_note == "E" and sharp_or_octave == "#":
        letter_note = "es"
    if letter_note == "F" and sharp_or_octave == "#":
        letter_note = "fs"
    if letter_note == "G" and sharp_or_octave == "#":
        letter_note = "gs"

    #Set to lowercase letters
    if letter_note =="A":
        letter_note = "a"
    if letter_note == "B":
        letter_note = "b"
    if letter_note =="C":
        letter_note = "c"
    if letter_note == "D":
        letter_note = "d"
    if letter_note == "E":
        letter_note="e"
    if letter_note == "F":
        letter_note = "f"
    if letter_note == "G":
        letter_note = "g"

    #Set Octave when sharp present
    if(sharp_or_octave =="#" and octave_check=="0"):
        octave=",,"
    if(sharp_or_octave =="#" and octave_check=="1"):
        octave=","
    if(sharp_or_octave =="#" and octave_check=="2"):
        octave=""
    if(sharp_or_octave =="#" and octave_check=="3"):
        octave="'"
    if(sharp_or_octave =="#" and octave_check=="4"):
        octave="''"
    if(sharp_or_octave =="#" and octave_check=="5"):
        octave="'''"
    if(sharp_or_octave =="#" and octave_check=="6"):
        octave="''''"
    if(sharp_or_octave =="#" and octave_check=="7"):
        octave="'''''"

    # Set Octave when sharp not present
    if(sharp_or_octave=="0"):
        octave=",,"
    if(sharp_or_octave=="1"):
        octave=","
    if(sharp_or_octave=="2"):
        octave=""
    if(sharp_or_octave =="3"):
        octave="'"
    if(sharp_or_octave =="4"):
        octave="''"
    if(sharp_or_octave =="5"):
        octave="'''"
    if(sharp_or_octave =="6"):
        octave="''''"
    if(sharp_or_octave =="7"):
        octave = "'''''"

    #set note duration default quarter note
    note_duration="4"

    #append note with helmholtz pitch notation
    notes.append(letter_note +  octave + note_duration)

#Remove duplicate notes and use Note and Container to display sheet music
output_list = []
i = 0
for item in notes:
    if (i == 0 or notes[i] != notes[i-1]):
        output_list.append(Note(notes[i]))
    i += 1
print(output_list)
container = Container(output_list)
show(container)



# # For each time slot, find the frequency with the highest amplitude
# # So F is a 1D array of length t
# F = (D_filtered.argmax(axis=0))
#
# # Map the frequency indexes to Hz
# for t in range(len(F)):
#     print(F[t])
#     F[t] = librosa.core.fft_frequencies()[F[t]]
#
# plt.plot(F)

#
#plt.plot(D.argmax(axis=0))