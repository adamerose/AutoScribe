import sys
from PyQt5 import QtWidgets, QtGui, QtCore
from PyQt5.QtWidgets import *
from PyQt5 import QtTest
import time
import threading
import librosa


# Note Object
class Create_Note():
    def __init__(self, letter, start_location):
        # Create a note object and set its parameters
        self.letter = letter
        self.start_location = start_location
        self.x = 0
        self.y = 0


# Window Object
class Music_Window(QWidget):  # QWidget
    # Define Class Variables
    note_id = 0  # Index for array of notes/lables
    destroy = 0

    C4_POSITION = 444
    DELTA_Y = 12.5
    DELTA_X = 13.734375
    RIGHT_SIDE = 932
    NOTE_SIZE = 40
    BORDER_PIXEL = 51  # Delete notes left of this pixel value
    WAIT_TIME = 500  # Wait time after add_note in ms

    def __init__(self):
        super(Music_Window, self).__init__()
        self.initUI()

    # Configure the default settings of the MusicWindow object
    def initUI(self):
        TEMPLATE_PATH = r'Resources\piano_roll.png'

        # Window Settings
        self.setWindowTitle('AutoScribe')
        self.setWindowIcon(QtGui.QIcon(r"Resources\logo.ico"))  # Icon
        self.setGeometry(100, 100, 942, 640)  # Windows size

        # Display a brackground image
        sheet_image = QtWidgets.QLabel(self)
        pixmap = QtGui.QPixmap(TEMPLATE_PATH)
        sheet_image.setPixmap(pixmap)
        sheet_image.move(0, 0)

        # Add buttons
        btn_reset = QtWidgets.QPushButton("Reset", self)
        btn_reset.move(5, 605)

        btn_export = QtWidgets.QPushButton("Export Sheet Music", self)
        btn_export.move(115, 605)

        btn_finished = QPushButton('Finish Recording', self)
        btn_finished.setToolTip('This is button will close the window and output the sheet music to a pdf')
        btn_finished.move(255, 605)
        btn_finished.clicked.connect(self.on_click)

        # Array of note labels and objects
        self.note_label_list = {}
        self.note_obj_list = {}

        self.show()

    def on_click(self):
        self.destroy = 1
        self.hide()
        print('Close Realtime Window')

    # Add a new label to the MusicWindow with the image and location that corresponds to the provided note object
    def add_note(self, new_notes):
        if (self.destroy == 0):

            # loop through previous notes and shift them to the left
            for i in self.note_label_list:
                self.note_obj_list[i].x -= self.DELTA_X
                self.note_label_list[i].move((self.note_obj_list[i].x), (self.note_obj_list[i].y))
                self.note_label_list[i].update()
                self.note_label_list[i].show()

                # remove image when it reaches the border_pixel mark
                if (self.note_obj_list[i].x <= self.BORDER_PIXEL):
                    self.note_label_list[i].hide()

            # Add new notes in given list
            for new_note in new_notes:
                new_note.x = self.RIGHT_SIDE - self.DELTA_X / 2  # intial x value
                new_note.y = 500  # intial y value (should always get overwritten)

                # Determine Y Location of the note
                new_note.y = self.C4_POSITION - self.DELTA_Y * self.note_delta('C4', new_note.letter)

                # Create the note label and store it in note_label_list
                self.note_label_list[Music_Window.note_id] = QtWidgets.QLabel(self)
                # select the image to display
                pixmap = QtGui.QPixmap(r'Resources\dot.png')
                pixmaptemp = pixmap.scaled(self.NOTE_SIZE, self.NOTE_SIZE,
                                           QtCore.Qt.KeepAspectRatio)  # resize image
                self.note_label_list[Music_Window.note_id].setPixmap(pixmaptemp)

                # Store the note object in the note_obj_list array.
                self.note_obj_list[Music_Window.note_id] = new_note
                self.note_obj_list[Music_Window.note_id].x = new_note.x - self.NOTE_SIZE / 2
                self.note_obj_list[Music_Window.note_id].y = new_note.y - self.NOTE_SIZE / 2

                # Update and render the new note
                self.note_label_list[Music_Window.note_id].move((self.note_obj_list[Music_Window.note_id].x),
                                                                (self.note_obj_list[Music_Window.note_id].y))
                self.note_label_list[Music_Window.note_id].update()
                self.note_label_list[Music_Window.note_id].show()

                # increment note counter and update window
                Music_Window.note_id += 1
                self.update()
                self.show()

            QtTest.QTest.qWait(self.WAIT_TIME)  # wait for wait_time in ms

    def note_delta(self, note1, note2):
        '''Return the number of semitones to get from note1 to note2'''
        return librosa.note_to_midi(note2) - librosa.note_to_midi(note1)


app = QtWidgets.QApplication(sys.argv)
window = Music_Window()

window.add_note([Create_Note("C3", 0)])
window.add_note([Create_Note("C4", 0)])
window.add_note([Create_Note("C5", 0)])
window.add_note([Create_Note("D#5", 0)])

window.add_note([Create_Note("C4", 0),
                 Create_Note("E4", 0),
                 Create_Note("G4", 0)])

window.add_note([])

window.add_note([Create_Note("C4", 0),
                 Create_Note("E4", 0),
                 Create_Note("A4", 0)])

window.add_note([])
window.add_note([])

window.add_note([Create_Note("C4", 0),
                 Create_Note("E4", 0),
                 Create_Note("G4", 0)])

window.add_note([Create_Note("C4", 0)])
window.add_note([Create_Note("C#4", 0)])
window.add_note([Create_Note("D4", 0)])
window.add_note([Create_Note("D#4", 0)])
