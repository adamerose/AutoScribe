import sys
from PyQt5 import QtWidgets, QtGui,QtCore
from PyQt5.QtWidgets import *

#Things to add:
#Add lines to the notes that not on the lines
#Add 8th/16th note images
#Treble vs Bass clef?
#Time signature?
#Tuplet, quintuplet, etc?


#Note Object
class Create_Note():
    def __init__(self, letter,start_location,duration):
        # Create a note object and set its parameters
        self.letter = letter
        self.start_location = start_location
        self.duration=duration
        self.x=0
        self.y=0

    def returnXY(self):
        return self.x, self.y

#Window Object
class Music_Window(QWidget):#QWidget
    #Define Class Variables
    note_id =0 #Index for array of notes/lables


    def __init__(self):
        super(Music_Window, self).__init__()
        self.title="Sheet Music Display"#Title
        self.initUI()

    #Configure the default settings of the MusicWindow object
    def initUI(self):
        # Window Settings
        self.setWindowTitle(self.title)#Title
        # music_window.setWindowIcon()#Icon
        self.setGeometry(100, 100, 1340,650)  # Windows size

        # Display a balnk sheet of music
        sheet_image = QtWidgets.QLabel(self)
        sheet_image.setPixmap(QtGui.QPixmap(r'Resources\blank_sheet.png'))

        #Array of note labels and objects
        self.note_list = {}
        self.note_obj= {}

    #Add a new label to the MusicWindow with the image and location that corresponds to the provided note object
    def add_note(self,new_note):

    #Determine the X and Y Location of the note
        #Determine X Location of the note
        if(new_note.start_location == 1):
            new_note.x = 100
        if(new_note.start_location == 2):
            new_note.x = 100+(new_note.start_location-1)*30
        if(new_note.start_location == 3):
            new_note.x = 100 + (new_note.start_location - 1) * 30
        if(new_note.start_location == 4):
            new_note.x = 100 + (new_note.start_location - 1) * 30
        if(new_note.start_location == 5):
            new_note.x = 100 + (new_note.start_location - 1) * 30
        if(new_note.start_location == 6):
            new_note.x = 100 + (new_note.start_location - 1) * 30
        if(new_note.start_location == 7):
            new_note.x = 100 + (new_note.start_location - 1) * 30
        if(new_note.start_location == 8):
            new_note.x = 100 + (new_note.start_location - 1) * 30
        if(new_note.start_location == 9):
            new_note.x = 100 + (new_note.start_location - 1) * 30
        if(new_note.start_location == 10):
            new_note.x = 100 + (new_note.start_location - 1) * 30
        if(new_note.start_location == 11):
            new_note.x = 100 + (new_note.start_location - 1) * 30
        if(new_note.start_location == 12):
            new_note.x = 100 + (new_note.start_location - 1) * 30
        if(new_note.start_location == 13):
            new_note.x = 100 + (new_note.start_location - 1) * 30
        if(new_note.start_location == 14):
            new_note.x = 100 + (new_note.start_location - 1) * 30
        if(new_note.start_location == 15):
            new_note.x = 100 + (new_note.start_location - 1) * 30

        #Determine Y Location of the note
        if (new_note.letter == "a'"):
            new_note.y = 72.5-2*6.5
        if(new_note.letter == "b'"):
            new_note.y = 72.5-3*6.5
        if(new_note.letter == "c'"):
            new_note.y = 72.5 -4*6.5
        if(new_note.letter == "d'"):
            new_note.y = 72.5 -5*6.5
        if(new_note.letter == "e'"):
            new_note.y = 72.5 -6*6.5
        if(new_note.letter == "f'"):
            new_note.y =72.5 -7*6.5
        if(new_note.letter == "g'"):
            new_note.y = 72.5 -8*6.5
        if (new_note.letter == "a''"):
            new_note.y = 72.5 -9*6.5
        if(new_note.letter == "b''"):
            new_note.y = 69 -10*6.5
        if(new_note.letter == "c''"):
            new_note.y = 69 -11*6.5
        if(new_note.letter == "d''"):
            new_note.y = 69 -12*6.5
        if(new_note.letter == "e''"):
            new_note.y = 69 -13*6.5
        if(new_note.letter == "f''"):
            new_note.y = 69 -14*6.5
        if(new_note.letter == "g''"):
            new_note.y = 69 -15*6.5
        if (new_note.letter == "a'''"):
            new_note.y = 69 - 16 * 6.5
        #new_note.y -=30
    #Create the note label and store it in note_list
        self.note_list[Music_Window.note_id] = QtWidgets.QLabel(self)

    #Set the Image, size, and adjust location if required
        #Set Image based on duration and y location (Handles Notes under middle C)
        if(new_note.duration == 4 and new_note.y >15):
            pixmap = QtGui.QPixmap(r'Resources\bot_quarter_note.png')
            pixmaptemp = pixmap.scaled(60, 60, QtCore.Qt.KeepAspectRatio)
            self.note_list[Music_Window.note_id].setPixmap(pixmaptemp)
            new_note.y +=-6.5

        if (new_note.duration == 8 and new_note.y>29):
            pixmap = QtGui.QPixmap(r'Resources\bot_8th_note.png')
            pixmaptemp = pixmap.scaled(64, 64, QtCore.Qt.KeepAspectRatio)
            self.note_list[Music_Window.note_id].setPixmap(pixmaptemp)
        if (new_note.duration == 16 and new_note.y>29):
            pixmap = QtGui.QPixmap(r'Resources\bot_16th_note.png')
            pixmaptemp = pixmap.scaled(64, 64, QtCore.Qt.KeepAspectRatio)
            self.note_list[Music_Window.note_id].setPixmap(pixmaptemp)
        #Set Image based on duration and y location (Handles Notes above middle C)
        if(new_note.y <= 10 and new_note.duration == 4):
            pixmap = QtGui.QPixmap(r'Resources\top_quarter_note.png')
            pixmaptemp = pixmap.scaled(60, 60, QtCore.Qt.KeepAspectRatio)
            self.note_list[Music_Window.note_id].setPixmap(pixmaptemp)
            new_note.y += 7*6.5-5
            new_note.x-=30
        if(new_note.y <= 28 and new_note.duration == 8):
            pixmap = QtGui.QPixmap(r'Resources\top_8th_note.png')
            pixmaptemp = pixmap.scaled(64, 64, QtCore.Qt.KeepAspectRatio)
            self.note_list[Music_Window.note_id].setPixmap(pixmaptemp)
            new_note.y += 7*6.5-5
            new_note.x-=30
        if(new_note.y <= -2.5 and new_note.duration == 16):
            pixmap = QtGui.QPixmap(r'Resources\top_16th_note.png')
            pixmaptemp = pixmap.scaled(64, 64, QtCore.Qt.KeepAspectRatio)
            self.note_list[Music_Window.note_id].setPixmap(pixmaptemp)
            new_note.y += 7*6.5-5
            new_note.x-=30

    #Set the final settings and display the new note.
        #Set the new xy position if changed
        self.note_list[Music_Window.note_id].move(new_note.x, new_note.y)
        #Store the note object in the note_obj array.
        self.note_obj[Music_Window.note_id] = new_note
        #Display the new note in the window
        self.note_list[Music_Window.note_id].show()

        self.note_id += 1
        self.hide()
        self.show()


