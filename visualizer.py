import pyaudio
import numpy as np
import matplotlib.pyplot as plt
import mytimer
mytimer = mytimer.MyTimer()
import time
# initialize stream parameters
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 44100
CHUNK = 1024 * 4

p = pyaudio.PyAudio()

stream = p.open(format=FORMAT,
                channels=CHANNELS,
                rate=RATE,
                input=True,
                frames_per_buffer=CHUNK)

fig, ax = plt.subplots()
x = np.arange(0, CHUNK, 1)
line, = ax.plot(x, np.random.rand(CHUNK))
ax.set_ylim(-150,150)


full_data = np.array([],dtype=np.int16)

while True:
    print()
    mytimer.tick("stream read")
    data = stream.read(CHUNK)

    mytimer.tick("data shaping")
    data_int = np.fromstring(data, dtype=np.int16)
    full_data = np.append(full_data,data_int)

    mytimer.tick("plotting")
    line.set_ydata(data_int)
    line.set_xdata( np.arange(0, len(data_int)) )
    ax.set_xlim(0, len(data_int))

    fig.canvas.draw()
    fig.canvas.flush_events()

    mytimer.tock()